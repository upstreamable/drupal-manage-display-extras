# Manage display extras

Additional features for the manage display module

https://www.drupal.org/project/manage_display/

## Features

* Adds a title formatter for string fields that accepts classes on the link.

## Contributions

Patches on drupal.org are accepted but merge requests on
[gitlab](https://gitlab.com/upstreamable/drupal-manage-display-extras) are preferred.
